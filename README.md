# FLISOL DISEÑO

## ToDo List

### General

- [X] Plantilla Días

### Canal BASH

- [X] El poder del software libre Jon "maddog" Hall
- [X] Uso de servicios de comunicación alterna y anonimos a los de Goolge
- [X] Taller de Linux Essentials-lpi
- [X] OnlyOffice - Su Nueva Souite de Oficina
- [X] Gobierno y el Sofware Libre
- [X] IoT - Prototipo Plug & Play
- [X] Implementa tu aplicación web en contenedor
- [X] WorkShop Instalación GNU/Linux

### Canal FISH

- [X] 7 Razones para utilizar software libre
- [X] Internet de las cosas para fortalecer la enseñanza de ciencias espaciales
- [X] Herramientas de gestión de granjas de impresión 3D
- [X] Introducción al Desarrollo de Exploits con Python (OSCP Style)
- [X] Ubuntu Touch, que necesitas para desarrollar apps moviles
- [X] Tu también puedes contribuir a proyectos de software libre
- [X] Acortando las brechas digitales de la empleabilidad, con software libre
- [X] Taller Jitsi Meet & OBS

### Canal ZSH

- [X] Taller criptomoneda Croat Coin
- [X] Ciencia ciudadania y calidad del aire
- [X] Mujeres y TIC: la brecha digital de género
- [X] RADIOGLUD
- [X] CoyIM cliente de Chat Seguro mediante Tor y OTR
- [X] Respuesta a incidentes: Un caso de phishing
- [X] Sequia de datos
- [X] Taller de Arduino

- [ ] Apertura

#### Link

[Link Tareas por hacer Drive](https://docs.google.com/spreadsheets/d/1CUn2Yt0oxd-oPjz8vRrOydUm0UnXJORRo5l0fBufYGU/edit?usp=sharing)
